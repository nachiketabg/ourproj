const express = require("express");
import firstRouter from "./src/routes/first";

const app: any = express();

app.use(firstRouter);

app.listen(3000, () => {
  console.log("Started");
});
