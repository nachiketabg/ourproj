const firstServices = require("../services/firstService");

module.exports = {
  post: async (req: any, resp: any) => {
    try {
      resp.send("HELLO POST!!");
    } catch (e) {}
  },
  get: async (req: any, resp: any) => {
    try {
      resp.send("HELLO");
    } catch (e) {}
  },
  patch: async (req: any, resp: any) => {
    try {
    } catch (e) {}
  },
  update: async (req: any, resp: any) => {
    try {
    } catch (e) {}
  },
  delete: async (req: any, resp: any) => {
    try {
    } catch (e) {}
  },
};
