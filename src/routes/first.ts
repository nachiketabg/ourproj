const express = require("express");
const firstController = require("../controllers/firstController");

//const { auth } = require("../Middleware/auth");

const firstRouter = new express.Router();

firstRouter.get("/first", firstController.get);
firstRouter.post("/first", firstController.post);
firstRouter.patch("/first", firstController.patch);
//firstRouter.update("/first", firstController.update);
firstRouter.delete("/first", firstController.delete);
export default firstRouter;
