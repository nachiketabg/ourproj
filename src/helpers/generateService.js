/**
 * 1. Create a line at router file inside routes with name given
 * 2. Create a file at controller file with name given
 * 3. Create a Folder at Services with given name
 */

var readline = require("readline");
var fs = require("fs");
var path = require("path");

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const getServiceName = () => {
  return new Promise((resolve, reject) => {
    rl.question("Enter The Service Name : ", (serviceName) => {
      resolve(serviceName);
    });
  });
};

const getPathName = () => {
  return new Promise((resolve, reject) => {
    rl.question("Enter The Path Name : ", (serviceName) => {
      resolve(serviceName);
    });
  });
};

const authRequired = () => {
  return new Promise((resolve, reject) => {
    rl.question("Is Auth Requied : Y/N ? ", (serviceName) => {
      if (!serviceName || serviceName == "Y") resolve(1);
      resolve(0);
    });
  });
};

const main = async () => {
  try {
    const serviceName = await getServiceName();
    const pathName = await getPathName();
    const isAuthRequired = await authRequired();

    fs.access(
      `${process.cwd()}/src/routes/${serviceName}.ts`,
      fs.F_OK,
      async (err) => {
        if (!err) {
          return console.log(
            `----------------------------------\nWARNING : ${serviceName} Already Exists !!!\n----------------------------------`
          );
        }

        await createRouter(serviceName, pathName, isAuthRequired);
        await createController(serviceName);
        await createService(serviceName);

        console.log(`\n-----------------------------------------------------------------------
ServiceName : ${serviceName} has been registered at Path : ${pathName}
-----------------------------------------------------------------------`);
      }
    );
    rl.close();
    return;
  } catch (e) {
    console.log("ERROR DURING CREATING OF SERVICE :", JSON.stringify(e));
    throw "ERROR DURING CREATING OF SERVICE";
  }
};

const createRouter = async (serviceName, pathName, isAuthRequired) => {
  const content = `const express = require("express");
const ${serviceName}Controller = require("../controllers/${serviceName}Controller");

const { auth } = require("../Middleware/auth");

const ${serviceName}Router = new express.Router();

${serviceName}Router.get("${pathName}", ${
    isAuthRequired ? `auth, ` : ``
  }${serviceName}Controller.get)
${serviceName}Router.post("${pathName}", ${
    isAuthRequired ? `auth, ` : ``
  }${serviceName}Controller.post)
${serviceName}Router.patch("${pathName}", ${
    isAuthRequired ? `auth, ` : ``
  }${serviceName}Controller.patch)
${serviceName}Router.update("${pathName}", ${
    isAuthRequired ? `auth, ` : ``
  }${serviceName}Controller.update)
${serviceName}Router.delete("${pathName}", ${
    isAuthRequired ? `auth, ` : ``
  }${serviceName}Controller.delete)
  export default ${serviceName}Router;`;
  fs.writeFile(
    `${process.cwd()}/src/routes/${serviceName}.ts`,
    content,
    function (err) {
      if (err) return console.log(err);
    }
  );
};

const createController = async (serviceName) => {
  const content = `const ${serviceName}Services = require("../services/${serviceName}Service");

module.exports = {
        post: async (req:any, resp:any) => {
            try {
    
            } catch (e) {
    
            }
          },
        get: async (req:any, resp:any) => {
            try {

            } catch (e) {

            }
          },
        patch: async (req:any, resp:any) => {
            try {

            } catch (e) {

            }
          },
        update: async (req:any, resp:any) => {
            try {

            } catch (e) {

            }
          },
        delete: async (req:any, resp:any) => {
            try {

            } catch (e) {

            }
          },
    
}`;
  fs.writeFile(
    `${process.cwd()}/src/controllers/${serviceName}Controller.ts`,
    content,
    function (err) {
      if (err) return console.log(err);
    }
  );
};

const createService = (serviceName) => {
  fs.writeFile(
    `${process.cwd()}/src/services/${serviceName}Service.ts`,
    "",
    function (err) {
      if (err) return console.log(err);
    }
  );
};

main();
